# Prototypage de Codex Médical pour l'Ukraine

Ce repo fait parti du projet [codexmedua](https://gitlab.com/codexmedua). L'objectif du repo est de produire un prototype rapide des fonctionnalités recherchées, de les expérimenter et de les documenter en vue d'une implémentation plus robuste.

## Pourquoi un 'codex'?

Le codex est la forme primitive du livre moderne qui a remplacé les rouleaux de papier. À l'époque (vers le Ve siècle) ce fut une invention importnante, notamment par ce que ce format facilitait l'accès à l'information à n'importe quelle partie d'un document.

De manière analogue, le projet codexmedua vise à rendre facilement disponible l'information dont ont besoin les professionnels de la santé en situation de crise.

## Contexte et enjeux

L'aide médicale en situation de crise a plusieurs particularités, dont celle-ci qui nous intéresserons particulièrement:

  - action urgente et rapide
  - besoin de coordonner les efforts
  - besoin de traduction technique

Concernant le codex, il y a 3 types d'opération:

  - Trouver et importer de l'information fiable
  - Stocker sous une forme structurée et utilisable
  - Fournir l'information pertinente à aux personnes qui en ont besoin


### Liste de besoin en fourniture médicale

  - [Liste des priorités médicales](https://moz.gov.ua/uploads/ckeditor/%D0%9D%D0%BE%D0%B2%D0%B8%D0%BD%D0%B8/Medical-supplies-priority%20needs.xlsx)

### Sources d'information/traduction médicale

  - [Glossaire en évaluation des technologies de la santé. Français, anglais, russe, allemand et espagnol](https://htaglossary.net/). Contient [une page web qui serait intéressante de scrapper](https://htaglossary.net/Liste+de+tous+les+termes#A). 
  - [Version russe du site de Vidal](https://www.vidal.ru/). Vidal est connu pour sont dictionnaire médical.
  - [Liste de vidal en français des substances actives](https://www.vidal.fr/medicaments/substances/liste-a.html)
  - [Explication (wikipedia) du système de classement ATC. Anatomical Therapeutic Chemical](https://fr.wikipedia.org/wiki/Classification_ATC)
  - [Info sur ATC/DDD de l'OMS](https://www.who.int/tools/atc-ddd-toolkit/about)
  - [Codes ATC de l'union européenne](https://ec.europa.eu/health/documents/community-register/html/reg_hum_atc.htm)
  - [SNOMED, terminologie médicale. Contient une ontology et des logiciel](https://www.snomed.org/)
  - [Merck index, documente les propriétés des molécules](https://www.rsc.org/merck-index)
  - [Information à partir du numéro CAS ou de la formule chimique. Chinois(?) et anglais.](https://www.chemsrc.com/en/)
  - [Drug Bank Online, information sur les médicament. Anglais](https://go.drugbank.com/drugs/DB00316)
  - [Drug Bank online, vocabulaire et structures sous forme csv](https://go.drugbank.com/releases/latest#open-data)
  - [ProZ. Glossaires thématiques anglais/ukranien](https://www.proz.com/glossary-translations/english-to-ukrainian-glossaries)
  - [Information de l'association internationnale des pharmaciens (FIP) pour l'invasion de l'ukraine par la Russie](https://www.fip.org/priorityareas-ukraine)
  - [Conversion csv de la traduction (anglais/ukrainien) des médicaments de FIP](https://github.com/franc00018/ukrainian_medicine_names_fip_csv)
  - [outils de conversion des posologies](https://ukrainemedlist.solutions.iqvia.com/)
  - 

### Google Sheets etc
  - [Lexique de Valentin Kravtchenko](https://docs.google.com/spreadsheets/d/1m3LampIajhMrl3ZrUytifBrHyt1YhOvYfSmnX0zH_Gk)
  

# Architecture

## Architecture haut niveau

![Architecture haut niveau](./haut-niveau.drawio.svg)


## Esquisse d'architecture 

![Esquisse d'architecture](./esquisse-architecture.svg)
